import { AddUserPage } from './../add-user/add-user';
import { InsurancePage } from './../insurance/insurance';
import { DoctorsPage } from './../doctors/doctors';
import { TabsPage } from './../tabs/tabs';
import { LoginPage } from './../login/login';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { analyzeAndValidateNgModules } from '@angular/compiler';
import { MenuController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { ModalController } from 'ionic-angular';
import {App} from 'ionic-angular';
/**
 * Generated class for the MenupagePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-menupage',
  templateUrl: 'menupage.html',
})
export class MenupagePage {
  userName:any;
  user:any;
  rootPage:any;
  userType: any;
  addingUser: any;
  userID: any;
  showDevice = false;
  constructor(public navCtrl: NavController,public modalCtrl: ModalController, public navParams: NavParams,private storage: Storage,private alertCtrl: AlertController,public menuCtrl: MenuController, public app: App) {

  }

  ionViewDidLoad() {
    // this.storage.get('userName').then((val) => {
    //   console.log('Your age is', val);
    //   if(val){
    //     this.user=val;
    //   }

    // });

    this.userName = this.navParams.get('uservalue');
    this.userType = this.navParams.get('usertype');
    this.userID = this.navParams.get('userid');

    if(this.userType == 'Patient'){
        this.rootPage = TabsPage;
        this.addingUser = 'Doctor';
        this.showDevice = true;
    }else if(this.userType == 'Doctor'){
      this.rootPage = DoctorsPage;
      this.addingUser = 'Patient';
      this.showDevice =false;
    }
    // else if(this.userName == 'insurance1'){
    //   this.rootPage = InsurancePage;
    // }

  }

  logout(){
    this.storage.remove('userName');
    this.app.getRootNav().setRoot(LoginPage);
    this.menuCtrl.close();
  }

  addUser() {
    let modal = this.modalCtrl.create(AddUserPage,{addinguser: this.addingUser,usertype: this.userType, userid: this.userID});
    console.log(this.addingUser)
    // this.navCtrl.push(modal);
    modal.present();
    this.menuCtrl.close();
    }

    addDevice() {
        this.navCtrl.push('AdddevicePage',{usertype: this.userType});
        this.menuCtrl.close();
    }



}
