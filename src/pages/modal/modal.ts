import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams,ViewController } from 'ionic-angular';

/**
 * Generated class for the ModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-modal',
  templateUrl: 'modal.html',
})
export class ModalPage {
  notifications = [
    {
      "notification_discri":"patient1 has high B.p",
     
    },
    {
      "notification_discri":"patient2 has high heart rate" 
    }
  ]
  constructor(public navCtrl: NavController, public navParams: NavParams,private view:ViewController) {
  }
  ionViewWillLoad() {
  let data= this.navParams.get('data');
   console.log(data);
  }

  closeModal() {
this.view.dismiss();
  }
 
}
