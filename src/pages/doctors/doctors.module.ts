import { AboutPage } from './../about/about';
import { HomePage } from './../home/home';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DoctorsPage } from './doctors';

@NgModule({
  declarations: [
    DoctorsPage,
    HomePage,
    AboutPage
  ],
  imports: [
    IonicPageModule.forChild(DoctorsPage),
  ],
})
export class DoctorsPageModule {}
