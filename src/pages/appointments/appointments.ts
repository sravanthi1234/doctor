import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams ,ModalController} from 'ionic-angular';
import { ModalPage } from '../modal/modal';


/**
 * Generated class for the AppointmentsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-appointments',
  templateUrl: 'appointments.html',
})
export class AppointmentsPage {

  constructor(public navCtrl: NavController,public modalCtrl: ModalController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AppointmentsPage');
  }
  onChange($event) {
    console.log($event);
  }
  openModal() {
    let myData={
      name:"sravanthi",
      occupation:"Ui"

    }
    let myModal = this.modalCtrl.create(ModalPage,{data:myData});
    myModal.present();
  }
  
}
