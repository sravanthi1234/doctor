import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AppointmentsPage } from './appointments';
import { DatePicker } from '@ionic-native/date-picker';
import { Component } from '@angular/core';

@NgModule({
  declarations: [
    AppointmentsPage,
  ],
  imports: [
    IonicPageModule.forChild(AppointmentsPage),
  ],
})
export class AppointmentsPageModule {
  date: string;
  type: 'string'; 
  
}
