import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ToastController } from 'ionic-angular';
import { HttpClient, HttpHeaders } from '@angular/common/http';

/**
 * Generated class for the AddUserPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@IonicPage()
@Component({
  selector: 'page-add-user',
  templateUrl: 'add-user.html',
})
export class AddUserPage {
  modalText: any;
  selectedItem: any;
  itemList: any;
  usertype:any;
  addingUser:any;
  items:any;
  loggedinUserid:any;
  constructor(public navCtrl: NavController, public navParams: NavParams,private view:ViewController, public http: HttpClient, public toastCtrl: ToastController) {
    this.addingUser= this.navParams.get('addinguser');
    this.usertype = this.navParams.get('usertype');
    this.loggedinUserid = this.navParams.get('userid');
    this.loadUsersList(this.usertype);
  }

  ionViewDidLoad() {


  }

  loadUsersList(type) {
    let body = {
      "usertype":type
    }

    let httpHeaders = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    let options = {
      headers: httpHeaders
    };

    this.http.post('http://ec2-54-84-142-57.compute-1.amazonaws.com:5000/user/getuserlist',JSON.stringify(body),options).subscribe((res)=> {
      this.items = res;
      this.initializeItems();
    })
  }

  selected() {
    this.view.dismiss(this.selectedItem);
    console.log(this.selectedItem);
    var body;
    if (this.addingUser === 'Doctor'){
       body = {
        "usertype":this.usertype,
        "userid":this.loggedinUserid,
        "selecteduserid":this.selectedItem.doctorid
      }
    }else if(this.addingUser === 'Patient'){
       body = {
        "usertype":this.usertype,
        "userid":this.loggedinUserid,
        "selecteduserid":this.selectedItem.patientid
      }
    }

    let httpHeaders = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    let options = {
      headers: httpHeaders
    };

    this.http.post('http://ec2-54-84-142-57.compute-1.amazonaws.com:5000/user/addpatientdoctor',JSON.stringify(body),options).subscribe((res)=> {
      let result = res;
      let message = result['message'];
      let toast = this.toastCtrl.create({
        message: message,
        duration: 3000,
        position: 'bottom'
      });

      toast.present();

    })

    console.log(JSON.stringify(body)+"new body");

  }

  select(item) {
    this.selectedItem = item;
  }

  initializeItems() {
    this.itemList = this.items;
  }
  getItems(ev: any) {
    // Reset items back to all of the items
    this.initializeItems();

    // set val to the value of the searchbar
    let val = ev.target.value;
    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      console.log(this.itemList);
      console.log(val);
      this.itemList = this.itemList.filter((item) => {

        if (this.addingUser === 'Doctor') {
          return (item.doctorname.toLowerCase().indexOf(val.toLowerCase()) > -1);
        } else if (this.addingUser === 'Patient') {
          return (item.patientname.toLowerCase().indexOf(val.toLowerCase()) > -1);
        }

      })
    }

  }
  closeModal() {
    this.view.dismiss();
      }
}
