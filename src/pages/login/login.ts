
import { InsurancePage } from "./../insurance/insurance";
import { TabsPage } from './../tabs/tabs';
import { Component, Injectable, Inject } from '@angular/core';
import { NavController, NavParams, AlertController, ModalController, LoadingController } from 'ionic-angular';
import { DoctorsPage } from '../doctors/doctors';
import { Storage } from '@ionic/storage';
import { PatientsPage } from '../patients/patients';
import { SignUpPage } from '../sign-up/sign-up';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ModalPage } from '../modal/modal';


@Component({
  selector: 'page-login',
  templateUrl: 'login.html',

})
export class LoginPage {
  username: string = "";
  password: string = "";
  loading = this.loadingCtrl.create({
    content: 'Please wait...'
  });
  constructor(public navCtrl: NavController, public alertCtrl: AlertController, public modalCtrl: ModalController, public navParams: NavParams, private storage: Storage, public http: HttpClient,public loadingCtrl: LoadingController) {
    this.navCtrl = navCtrl
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  login() {

    if (this.username == "" || this.password == "") {
      let userAlert = this.alertCtrl.create({

        subTitle: 'Please enter username and Password',
        buttons: ['OK']
      });
      userAlert.present();
    }else {
      this.loading.present();
      let body = {
        "emailid": this.username,
        "password": this.password
      }

      let httpHeaders = new HttpHeaders({
        'Content-Type': 'application/json'
      });
      let options = {
        headers: httpHeaders
      };

    this.http.post('http://ec2-54-84-142-57.compute-1.amazonaws.com:5000/user/authenticate',JSON.stringify(body),options).subscribe(res => {
      console.log(JSON.stringify(res));
      this.loading.dismiss();
      let userType = res["uertype"];
      let userEmail = res["emailid"];
      let userId = res["userid"];
      this.storage.set('useremail',userEmail);
      if(res["success"] == true){
          if(userType == "Patient"){
            this.navCtrl.setRoot('MenupagePage', { uservalue: userEmail , usertype : userType, userid: userId});
          }else if(userType == "Doctor"){
            this.navCtrl.setRoot('MenupagePage', { uservalue: userEmail , usertype : userType, userid: userId});
          }
      } else {
        let basicAlert = this.alertCtrl.create({

          subTitle: res["msg"],
          buttons: ['OK']
        });
        basicAlert.present();
      }
    })

  }


    if (this.username == "doctor1" && this.password == "doctor1") {

      this.storage.set("userName", this.username);
      this.navCtrl.setRoot('MenupagePage', { uservalue: this.username });


    }
    else if (this.username == "insurance1" && this.password == "insurance1") {
      // this.navCtrl.setRoot(InsurancePage);
      this.storage.set("userName", this.username);
      this.navCtrl.setRoot('MenupagePage', { uservalue: this.username });

    }
  }
  signup() {
    this.navCtrl.push(SignUpPage)
  }

}
