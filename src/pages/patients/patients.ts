import { AboutPage } from './../about/about';
import { Component } from '@angular/core';
import { IonicPage, NavController, ModalController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { ModalPage } from '../modal/modal';


/**
 * Generated class for the PatientsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-patients',
  templateUrl: 'patients.html',
})
export class PatientsPage {

  patients = [
    {
      "patient_name":"Patient1",
      "profile_image":"assets/images/pp.jpg",
      "green_icon": "assets/images/green.png",
      "patient_type":"Heart Patient",
      "location":"Idano,U.S.A",
      "send_icon":"assets/images/send.png"
    },
    {
      "patient_name":"Patient2",
      "profile_image":"assets/images/pp.jpg",
      "green_icon": "assets/images/green.png",
      "patient_type":"Heart Patient",
      "location":"Idano,U.S.A",
      "send_icon":"assets/images/send.png"
    },
    {
      "patient_name":"Patient3",
      "profile_image":"assets/images/pp.jpg",
      "green_icon": "assets/images/red.png",
      "patient_type":"Heart Patient",
      "location":"Idano,U.S.A",
      "send_icon":"assets/images/send.png"
    }
  ]
  constructor(public navCtrl: NavController, public modalCtrl: ModalController, public alertCtrl: AlertController) {
  }
  showPrompt() {
    const prompt = this.alertCtrl.create({
      title: 'Send message',
       // message: "Enter a name for this new album you're so keen on adding",
      inputs: [
        {
          name: 'title',
          placeholder: 'send message'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Send',
          handler: data => {
            console.log('Saved clicked');
          }
        }
      ]
    });
    prompt.present();
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad PatientsPage');
  }

  gotoProfile(patient) {
      console.log(patient+"hiii");
        this.navCtrl.push('PatientProfilePage');
  }

  openModal() {
    let myData={
      name:"sravanthi",
      occupation:"Ui"

    }
    let myModal = this.modalCtrl.create(ModalPage,{data:myData});
    myModal.present();
  }
}
