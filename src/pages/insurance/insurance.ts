import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AppointmentsPage } from '../appointments/appointments';
import { PatientsPage } from '../patients/patients';
import { PatientInsurancePage } from '../patient-insurance/patient-insurance';
import { DoctorInsurancePage } from '../doctor-insurance/doctor-insurance';

/**
 * Generated class for the InsurancePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-insurance',
  templateUrl: 'insurance.html',
})
export class InsurancePage {
  tab1Root = PatientInsurancePage;
  tab2Root = DoctorInsurancePage;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad InsurancePage');
  }

}
