import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { InsurancePage } from './insurance';
import { AboutPage } from '../about/about';
import { HomePage } from '../home/home';

@NgModule({
  declarations: [
    InsurancePage,
    HomePage,
    AboutPage
  ],
  imports: [
    IonicPageModule.forChild(InsurancePage),
  ],
})
export class InsurancePageModule {}
