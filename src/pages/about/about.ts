import { Component } from '@angular/core';
import { NavController,ModalController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ModalPage } from '../modal/modal';

@Component({
  selector: 'page-about',
  templateUrl: 'about.html'

})
export class AboutPage {
  userName:any;
  useremail:any;
  userarray:any;
  userage: any;
  patientType: any;
  heartbeat:any;
  temperature:any;

  constructor(public navCtrl: NavController,public modalCtrl: ModalController,private storage: Storage,public http: HttpClient) {
    this.storage.get('useremail').then((val) => {
      this.useremail = val;
      this.loadpatientsList(this.useremail);
    });
    this.storage.get('heartbeat').then((val) => {
      this.heartbeat = val;
    });
    this.storage.get('temperature').then((val) => {
      this.temperature = val; 
    });
    
  }
  openModal() {
    let myData={
      name:"sravanthi",
      occupation:"Ui"

    }
    let myModal = this.modalCtrl.create(ModalPage,{data:myData});
    myModal.present();
  }
  loadpatientsList(email) {
    let body = {
      "usertype":"Doctor"
    }

    let httpHeaders = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    let options = {
      headers: httpHeaders
    };

    this.http.post('http://ec2-54-84-142-57.compute-1.amazonaws.com:5000/user/getuserlist',JSON.stringify(body),options).subscribe((res)=> {
      console.log(res);
      this.userarray = res;
      for(let i=0;i < this.userarray.length;i++){
        if(email == this.userarray[i]['emailid']){
            this.userName = this.userarray[i]['patientname'];
            this.userage = this.userarray[i]['age'];
            this.patientType = this.userarray[i]['patienttype'];
            console.log(this.userName+""+this.userage);
        }
      }
    })
  }

  }


