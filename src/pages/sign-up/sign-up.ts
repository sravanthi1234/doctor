import { LoginPage } from './../login/login';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormControl, FormGroup, Validators, ValidatorFn, AbstractControl, FormBuilder } from '@angular/forms';
import { HttpClient, HttpHeaders  } from '@angular/common/http';
import { ToastController } from 'ionic-angular';


/**
 * Generated class for the SignUpPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-sign-up',
  templateUrl: 'sign-up.html',
})
export class SignUpPage {

  doctor: any;
  patient: any;
  usertype: any;
  buttonColor: any;
  buttonColor1: any;
  email: any;
  name: any;
  mobile: any;
  password: any;
  confirmPassword: any;
  contactNumber: any;
  qualification: any;
  age: any;
  gender: any;
patientType:any;

  showDoctor(value) {
    this.doctor = true;
    this.patient = false;
    this.usertype = value;
    console.log(this.usertype);
    if (this.doctor == true) {
      this.buttonColor = '#f6d915';
      this.buttonColor1 = 'transparent'
    }
    else {
      this.buttonColor1 = 'transparent';
    }


  }
  showPatient(value) {
    this.patient = true;
    this.doctor = false;
    this.usertype = value;
    console.log(this.usertype);
    if (this.patient == true) {
      this.buttonColor1 = '#f6d915';
      this.buttonColor = 'transparent';
    }
    else {
      this.buttonColor = 'transparent';
    }


  }



  validation_messages = {
    'email': [
      { type: 'required', message: 'email is required.' },
      { type: 'pattern', message: 'Please include @ in email like example@gmail.com' },
    ],
    'name': [
      { type: 'required', message: 'Username is required.' },

    ],
    'patientType': [
      { type: 'required', message: 'Patient Type is required.' },

    ],
    'mobileNumber': [
      { type: 'required', message: 'mobile number is required.' },
      { type: 'maxlength', message: 'please enter 10 digits only.' },
      { type: 'pattern', message: 'please enter numbers only' }


    ],
    'age': [
      { type: 'required', message: 'Age is is required.' }


    ],


    'contactNumber': [
      { type: 'required', message: 'Age is is required.' },
      { type: 'maxlength', message: 'please enter 10 digits only.' },
      { type: 'pattern', message: 'please enter numbers only' }


    ],
  }
  doctor_messages = {
    'emails': [
      { type: 'required', message: 'email is required.' },
      { type: 'pattern', message: 'Please include @ in email like example@gmail.com' },
    ],
    'names': [
      { type: 'required', message: 'Username is required.' },

    ],
    'mobileNumbers': [
      { type: 'required', message: 'mobile number is required.' },
      { type: 'maxlength', message: 'please enter 10 digits only.' },
      { type: 'pattern', message: 'please enter numbers only' }


    ],
    
    'qualification': [
      { type: 'required', message: 'qualification is required.' }


    ],
    'specialization': [
      { type: 'required', message: 'specialization  is required.' }


    ],
    'expierence': [
      { type: 'required', message: 'experence is required.' }


    ],
    'ages': [
      { type: 'required', message: 'age  is required.' }


    ],
    'awards': [
      { type: 'required', message: 'awards  is required.' }


    ],

    'contactNumbers': [
      { type: 'required', message: 'Contact Number  is required.' },
      { type: 'maxlength', message: 'please enter 10 digits only.' },
      { type: 'pattern', message: 'please enter numbers only' }


    ],
  }

  public todo: FormGroup;
  public todoNew: FormGroup;
  constructor(public navCtrl: NavController, public navParams: NavParams, private formBuilder: FormBuilder, public http: HttpClient,public toastCtrl: ToastController) {
    this.todo = this.formBuilder.group({
      email: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      ])),
      name: new FormControl('', Validators.compose([
        Validators.required

      ])),
      patientType: new FormControl('', Validators.compose([
        Validators.required

      ])),

      mobileNumber: new FormControl('', Validators.compose([
        Validators.required,
        Validators.maxLength(10),
        Validators.pattern("^[1-9][0-9]*")


      ])),


      password: new FormControl('', [Validators.required]),
      re_password: new FormControl('', [Validators.required, this.equalto('password')]),

      age: new FormControl('', Validators.compose([
        Validators.required

      ])),




      contactNumber: new FormControl('', Validators.compose([
        Validators.required,
        Validators.maxLength(10),
        Validators.pattern("^[1-9][0-9]*")

      ])),

      gender: [],

    },

    );

    this.todoNew = this.formBuilder.group({
      emails: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      ])),
      names: new FormControl('', Validators.compose([
        Validators.required

      ])),
     
      

      passwords: new FormControl('', [Validators.required]),
      qualification: new FormControl('', [Validators.required]),
      specialization: new FormControl('', [Validators.required]),
      expierence: new FormControl('', [Validators.required]),
      awards: new FormControl('', [Validators.required]),
      re_passwords: new FormControl('', [Validators.required, this.equalto('passwords')]),

      ages: new FormControl('', Validators.compose([
        Validators.required

      ])),




     
      
      gender: [],

    },

    );

  }
  
 

  equalto(field_name): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {

      let input = control.value;

      let isValid = control.root.value[field_name] == input
      if (!isValid)
        return { 'equalTo': { isValid } }
      else
        return null;
    };
  }

  logForm() {

    console.log(this.todo.value)
    let body = {
			      "usertype":this.usertype,
            "name": this.todo.value.name,
            "password":this.todo.value.re_password,
            "gender": this.todo.value.gender,
            "age": parseInt(this.todo.value.age),
            "emailid": this.todo.value.email,
            "patienttype":this.todo.value.patientType,
            "qualification": "",
            "specialization": "",
            "experience": "",
            "awards": "",
            "address":"Hyderabad",
            "phone":parseInt(this.todo.value.mobileNumber),
            "deviceid":10,
           "emergencycontact":parseInt(this.todo.value.contactNumber),
            "syscreatedby": "Patient",
            "sysupdatedby": "Patient",
            "versionnumber": 2
        }
    console.log(body);
    let httpHeaders = new HttpHeaders({
      'Content-Type' : 'application/json'
   });
   let options = {
    headers: httpHeaders
    };

    this.http.post('http://ec2-54-84-142-57.compute-1.amazonaws.com:5000/user/register',JSON.stringify(body),options).subscribe(res => {
      console.log(JSON.stringify(res["message"]));
      let responseMessage = res["message"];
      if(responseMessage == "user resgistered succesfully"){
        let toast = this.toastCtrl.create({
          message: responseMessage,
          duration: 3000,
          position: 'bottom'
        });

        toast.onDidDismiss(() => {
          this.navCtrl.setRoot(LoginPage);
        });

        toast.present();
      }else{

      }
    })



  }
  anotherForm() {
    console.log(this.todoNew.value)
    let body = {
      "usertype":this.usertype,
      "name": this.todoNew.value.names,
      "password":this.todoNew.value.re_passwords,
      "gender": this.todoNew.value.gender,
      "age": parseInt(this.todoNew.value.ages),
      "emailid": this.todoNew.value.emails,
      "patienttype":"",
      "qualification": this.todoNew.value.qualification,
      "specialization": this.todoNew.value.specialization,
      "experience": this.todoNew.value.expierence,
      "awards": this.todoNew.value.awards,
      "address":"",
      "phone":"",
      "deviceid":"",
     "emergencycontact":"",
      "syscreatedby": "Doctor",
      "sysupdatedby": "Doctor",
      "versionnumber": 2
  }
console.log(body);
let httpHeaders = new HttpHeaders({
  'Content-Type' : 'application/json'
});
let options = {
headers: httpHeaders
};

this.http.post('http://ec2-54-84-142-57.compute-1.amazonaws.com:5000/user/register',JSON.stringify(body),options).subscribe(res => {
  console.log(JSON.stringify(res["message"]));
  let responseMessage = res["message"];
  if(responseMessage == "user resgistered succesfully"){
    let toast = this.toastCtrl.create({
      message: responseMessage,
      duration: 3000,
      position: 'bottom'
    });

    toast.onDidDismiss(() => {
      this.navCtrl.setRoot(LoginPage);
    });

    toast.present();
  }else{

  }
})



  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad SignUpPage');
  }

}
