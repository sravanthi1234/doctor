import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ModalController } from 'ionic-angular';
import { ModalPage } from '../modal/modal';
/**
 * Generated class for the PatientInsurancePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-patient-insurance',
  templateUrl: 'patient-insurance.html',
})
export class PatientInsurancePage {

  constructor(public navCtrl: NavController, public navParams: NavParams,public modalCtrl: ModalController,) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PatientInsurancePage');
  }
  openModal() {
    let myData={
      name:"sravanthi",
      occupation:"Ui"

    }
    let myModal = this.modalCtrl.create(ModalPage,{data:myData});
    myModal.present();
  }
}
