import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PatientInsurancePage } from './patient-insurance';

@NgModule({
  declarations: [
    PatientInsurancePage,
  ],
  imports: [
    IonicPageModule.forChild(PatientInsurancePage),
  ],
})
export class PatientInsurancePageModule {}
