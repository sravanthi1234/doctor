import { Component, ViewChild, NgZone } from '@angular/core';
import { NavController,ModalController } from 'ionic-angular';
import { Chart } from 'chart.js';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Storage } from '@ionic/storage';
import { ModalPage } from '../modal/modal';
// import * as CanvasJS from '../../assets/canvasjs-2.3.1/canvasjs.min';
// import * as CanvasJS from '../../assets/canvasjs-2.3.1/canvasjs.min';
declare var CanvasJS: any;
@Component({
    selector: 'page-home',
    templateUrl: 'home.html'
})
export class HomePage {
 @ViewChild('doughnutCanvas') doughnutCanvas;
    @ViewChild('lineCanvas') lineCanvas;
    @ViewChild('bloodPressure') bloodPressure;
    @ViewChild('tempurature') tempurature;
// doughnutChart: any;
    lineChart: any;
    interval:any;
    users: any;
    heartBeat: any;
    temperatures: any;
    myChart: any;
    heartArray = [];
    tempArray=[];
    abnormalcount = 0;
    alerts= [];
    constructor(public navCtrl: NavController, public modalCtrl: ModalController,public http: HttpClient, public ngZone: NgZone, public storage: Storage) {

    }
    ionViewDidEnter() {


    }
    ngOnInit() {
        var xAxisStripLinesArray = [];
        var yAxisStripLinesArray = [];
        var dps = [];
        var dataPointsArray = [1835,1967,1956,1952,1952,1919,1886,1886,1883,1883,1876,1859,1859,1838,1844,1844,1838,1823,1823,1823,1850,1850,1817,1824,1824,1846,1835,1835,1838,1838,1853,1853,1853,1853,1862,1862,1858,1872,1872,1876,1874,1874,1883,1896,1896,1911,1911,1910,1913,1913,1906,1935,1935,1933,1934,1934,1936,1944,1944,1943,1948,1948,1939,1957,1957,1963,1954,1954,1954,1954,1971,1971,1971,1967,1960,1960,1970,1976,1976,1971,1962,1962,1978,1960,1960,1957,1957,1972,1966,1966,1966,1960,1960,1955,1968,1968,1970,1956,1956,1971,1972,1972,1959,1972,1972,1991,1978,1978,1967,1967,1966,1979,1979,1973,1968,1968,1979,1977,1977,1960,1970,1970,1975,1964,1964,1968,1968,1969,1960,1960,1971,1970,1970,1974,1975,1975,1967,1960,1960,1976,1974,1974,1983,1978,1978,1974,1974,1985,1982,1982,1974,1984,1984,1983,1984,1984,1992,1987,1987,1987,1987,1987,1992,1992,1987,1996,1996,1996,1992,1992,1994,1992,1992,2004,2001,2001,2004,1998,1998,1991,2005,2005,2022,2022,2005,2007,2007,2011,2012,2012,2008,2016,2016,2021,2024,2024,2005,2011,2011,2019,2019,2014,2015,2015,2014,1999,1999,2011,2013,2013,1999,2024,2024,2008,1996,1996,2027,2011,2011,2010,2010,2036,2023,2023,2028,2045,2045,2050,2051,2051,2059,2054,2054,2067,2056,2056,2047,2066,2066,2049,2049,2035,2041,2041,2013,2020,2020,2014,1998,1998,1990,1992,1992,1987,1980,1980,1979,1985,1985,1980,1980,1981,1992,1992,1974,1987,1987,1977,1981,1981,1983,1987,1987,2013,2040,2040,2083,2130,2130,2183,2183,2250,2325,2325,2423,2527,2527,2559,2559,2559,2523,2368,2368,2232,2096,2096,2004,2004,1901,1858,1858,1834,1838,1838,1831,1853,1853,1847,1845,1845,1851,1860,1860,1870,1872,1872,1891,1893,1893,1894,1894,1901,1919,1919,1917,1930,1930,1947,1925,1925,1943,1952,1952,1947,1954,1954,1951,1951,1960,1962,1962,1966,1978,1978,1979,1973,1973,1984,1978,1978,1984,1991,1991,1983,1983,1983,1999,1992,1992,1998,1998,1998,2006,2006,1999,2016,2016,2005,2038,2038,2006,2017,2017,2042,2027,2027,2031,2031,2051,2044,2044,2043,2070,2070,2062,2055,2055,2065,2056,2056,2070,2080,2080,2068,2078,2078,2077,2077,2074,2087,2087,2093,2102,2102,2091,2102,2102,2118,2114,2114,2116,2128,2128,2108,2108,2095,2108,2108,2102,2096,2096,2094,2081,2081,2074,2082,2082,2088,2059,2059,2058,2050,2050,2027,2027,2014,2009,2009,1991,1981,1981,1951,1949,1949,1932,1918,1918,1923,1893,1893,1885,1885,1894,1882,1882,1871,1890,1890,1865,1868,1868,1874,1876,1876,1887,1884,1884,1886,1888,1888,1895,1902,1902,1898,1898,1898,1906,1906,1911,1914,1914,1918,1922,1922,1918,1936,1936,1950,1937,1937,1944,1944,1955,1947,1947,1951,1984,1984,1950,1972,1972,1974,1958,1958,1982,1971,1971,1983,1974,1974,1967,1986,1986,1979,1979,1970,1986,1986,1966,1955,1955,1970,1976,1976,1972,1968,1968,1970,1969,1969,1967,1967,1998,1976,1976,1981,1994,1994,1982,1987,1987,1987,1996,1996,1983,1974,1974,1991,1991,1982,1984,1984,2004,1982,1982,1983,2009,2009,1995,1991,1991,2001,2000,2000,1991,1999,1999,2002,1988,1988,1999,1999,1979,2006,2006,2005,1983,1983,1995,1986,1986,1992,2010,2010,1993,2000,2000,1994,1994,1990,2012,2012,1989,1993,1993,1993,1986,1986,1999,1986,1986,2002,1995,1995,1995,1993,1993,2000,2000,1996,1997,1997,1995,1998,1998,1983,1997,1997,1998,1990,1990,1993,1996,1996,1982,1995,1995,2003,2003,2003,1996,1996,1993,1989,1989,2007,2008,2008,2006,2004,2004,1997,2000,2000,2012,2007,2007,2013,2013,2010,2004,2004,2010,2011,2011,2011,2015,2015,2015,2016,2016,2011,2005,2005,2014,2014,2008,1998,1998,2021,2009,2009,2007,2018,1835,1967,1956,1952,1952,1919,1886,1886,1883,1883,1876,1859,1859,1838,1844,1844,1838,1823,1823,1823,1850,1850,1817,1824,1824,1846,1835,1835,1838,1838,1853,1853,1853,1853,1862,1862,1858,1872,1872,1876,1874,1874,1883,1896,1896,1911,1911,1910,1913,1913,1906,1935,1935,1933,1934,1934,1936,1944,1944,1943,1948,1948,1939,1957,1957,1963,1954,1954,1954,1954,1971,1971,1971,1967,1960,1960,1970,1976,1976,1971,1962,1962,1978,1960,1960,1957,1957,1972,1966,1966,1966,1960,1960,1955,1968,1968,1970,1956,1956,1971,1972,1972,1959,1972,1972,1991,1978,1978,1967,1967,1966,1979,1979,1973,1968,1968,1979,1977,1977,1960,1970,1970,1975,1964,1964,1968,1968,1969,1960,1960,1971,1970,1970,1974,1975,1975,1967,1960,1960,1976,1974,1974,1983,1978,1978,1974,1974,1985,1982,1982,1974,1984,1984,1983,1984,1984,1992,1987,1987,1987,1987,1987,1992,1992,1987,1996,1996,1996,1992,1992,1994,1992,1992,2004,2001,2001,2004,1998,1998,1991,2005,2005,2022,2022,2005,2007,2007,2011,2012,2012,2008,2016,2016,2021,2024,2024,2005,2011,2011,2019,2019,2014,2015,2015,2014,1999,1999,2011,2013,2013,1999,2024,2024,2008,1996,1996,2027,2011,2011,2010,2010,2036,2023,2023,2028,2045,2045,2050,2051,2051,2059,2054,2054,2067,2056,2056,2047,2066,2066,2049,2049,2035,2041,2041,2013,2020,2020,2014,1998,1998,1990,1992,1992,1987,1980,1980,1979,1985,1985,1980,1980,1981,1992,1992,1974,1987,1987,1977,1981,1981,1983,1987,1987,2013,2040,2040,2083,2130,2130,2183,2183,2250,2325,2325,2423,2527,2527,2559,2559,2559,2523,2368,2368,2232,2096,2096,2004,2004,1901,1858,1858,1834,1838,1838,1831,1853,1853,1847,1845,1845,1851,1860,1860,1870,1872,1872,1891,1893,1893,1894,1894,1901,1919,1919,1917,1930,1930,1947,1925,1925,1943,1952,1952,1947,1954,1954,1951,1951,1960,1962,1962,1966,1978,1978,1979,1973,1973,1984,1978,1978,1984,1991,1991,1983,1983,1983,1999,1992,1992,1998,1998,1998,2006,2006,1999,2016,2016,2005,2038,2038,2006,2017,2017,2042,2027,2027,2031,2031,2051,2044,2044,2043,2070,2070,2062,2055,2055,2065,2056,2056,2070,2080,2080,2068,2078,2078,2077,2077,2074,2087,2087,2093,2102,2102,2091,2102,2102,2118,2114,2114,2116,2128,2128,2108,2108,2095,2108,2108,2102,2096,2096,2094,2081,2081,2074,2082,2082,2088,2059,2059,2058,2050,2050,2027,2027,2014,2009,2009,1991,1981,1981,1951,1949,1949,1932,1918,1918,1923,1893,1893,1885,1885,1894,1882,1882,1871,1890,1890,1865,1868,1868,1874,1876,1876,1887,1884,1884,1886,1888,1888,1895,1902,1902,1898,1898,1898,1906,1906,1911,1914,1914,1918,1922,1922,1918,1936,1936,1950,1937,1937,1944,1944,1955,1947,1947,1951,1984,1984,1950,1972,1972,1974,1958,1958,1982,1971,1971,1983,1974,1974,1967,1986,1986,1979,1979,1970,1986,1986,1966,1955,1955,1970,1976,1976,1972,1968,1968,1970,1969,1969,1967,1967,1998,1976,1976,1981,1994,1994,1982,1987,1987,1987,1996,1996,1983,1974,1974,1991,1991,1982,1984,1984,2004,1982,1982,1983,2009,2009,1995,1991,1991,2001,2000,2000,1991,1999,1999,2002,1988,1988,1999,1999,1979,2006,2006,2005,1983,1983,1995,1986,1986,1992,2010,2010,1993,2000,2000,1994,1994,1990,2012,2012,1989,1993,1993,1993,1986,1986,1999,1986,1986,2002,1995,1995,1995,1993,1993,2000,2000,1996,1997,1997,1995,1998,1998,1983,1997,1997,1998,1990,1990,1993,1996,1996,1982,1995,1995,2003,2003,2003,1996,1996,1993,1989,1989,2007,2008,2008,2006,2004,2004,1997,2000,2000,2012,2007,2007,2013,2013,2010,2004,2004,2010,2011,2011,2011,2015,2015,2015,2016,2016,2011,2005,2005,2014,2014,2008,1998,1998,2021,2009,2009,2007,2018];
        
        
        var chart = new CanvasJS.Chart("chartContainer",
            {
                  title:{
                  text:"ECG Report",
              },
              subtitles:[{
                  text: "Patient Name: Patient Name",
                  horizontalAlign: "left",
                },
                {
                  text: "Age: X-Years",
                  horizontalAlign: "left",
                },
                
                    ],
              axisY:{
                  stripLines:yAxisStripLinesArray,
                gridThickness: 2,
                gridColor:"#DC74A5",
                lineColor:"#DC74A5",
                tickColor:"#DC74A5",
                labelFontColor:"#DC74A5",        
              },
              axisX:{
                  stripLines:xAxisStripLinesArray,
                gridThickness: 2,
                gridColor:"#DC74A5",
                lineColor:"#DC74A5",
                tickColor:"#DC74A5",
                labelFontColor:"#DC74A5",
              },
              data: [
              {
                type: "spline",
                color:"black",
                dataPoints: dps
              }
              ]
            });
          
        addDataPointsAndStripLines();
        chart.render();
          
        function addDataPointsAndStripLines(){
                //dataPoints
            for(var i=0; i<dataPointsArray.length;i++){
                dps.push({y: dataPointsArray[i]});
            }
            //StripLines
            for(var i=0;i<3000;i=i+100){
              if(i%1000 != 0)
                  yAxisStripLinesArray.push({value:i,thickness:0.7, color:"#DC74A5"});  
            }
            for(var i=0;i<1400;i=i+20){
              if(i%200 != 0)
                  xAxisStripLinesArray.push({value:i,thickness:0.7, color:"#DC74A5"});  
            }
        }
	
        this.getUsers();
        this.interval = setInterval(() => {
            this.ngZone.run(() => {
                this.getUsers();
            });
        }, 5000);


    }

    ionViewWillLeave() {
        // this.ngZone.run(() => {
        //     clearInterval(this.interval);
        // });
    }
    openModal() {
      this.alerts = [];
        let myData={
          name:"sravanthi",
          occupation:"Ui"

        }
        let myModal = this.modalCtrl.create(ModalPage,{data:myData});
        myModal.present();
      }
    getUsers() {
        this.heartArray=[];
        this.tempArray=[];
        let body = {
          "patientkey":"10"
        }

        let httpHeaders = new HttpHeaders({
          'Content-Type': 'application/json'
        });
        let options = {
          headers: httpHeaders
        };
        this.http.post('http://ec2-54-84-142-57.compute-1.amazonaws.com:8080/devices/getdevicedata',JSON.stringify(body),options).subscribe(data => {

            this.users = data;
            for (let i = 0; i < this.users.length; i++) {
                this.heartArray.push(this.users[i].heartbeat);
            }
            for (let i = 0; i < this.users.length; i++) {
                this.tempArray.push(this.users[i].temperature);
            }
            console.log(this.users[this.users.length - 1]);
            let result = this.users[0];
            console.log(result.heartbeat);

            this.heartBeat = result.heartbeat;

            this.temperatures = result.temperature;
            this.storage.set('heartbeat',this.heartBeat);
            this.storage.set('temperature',this.temperatures);
            this.chartData(this.heartBeat, this.heartArray, this.temperatures,this.tempArray);
        })
    }

    chartData(heart, heartarray, temper,tempArray) {
      for(var i=0; i< heartarray.length; i++){
        if(this.heartArray[i] < 60 || this.heartArray[i] > 100){
          this.abnormalcount++;
        }
        if(this.abnormalcount === 3){
          this.abnormalcount = 0;
          let alert = 1;
          this.alerts.push(alert);
          break;
        }
      }
        Chart.pluginService.register({
            beforeDraw: function (chart) {
                if (chart.config.options.elements.center) {
                    //Get ctx from string
                    var ctx = chart.chart.ctx;

                    //Get options from the center object in options
                    var centerConfig = chart.config.options.elements.center;
                    var fontStyle = centerConfig.fontStyle || 'Arial';
                    var txt = centerConfig.text;
                    var color = centerConfig.color || '#000088';
                    var sidePadding = centerConfig.sidePadding || 2;
                    var sidePaddingCalculated = (sidePadding / 100) * (chart.innerRadius * 2)
                    //Start with a base font of 30px
                    ctx.font = "30px " + fontStyle;

                    //Get the width of the string and also the width of the element minus 10 to give it 5px side padding
                    var stringWidth = ctx.measureText(txt).width;
                    var elementWidth = (chart.innerRadius * 2) - sidePaddingCalculated;

                    // Find out how much the font can grow in width.
                    var widthRatio = elementWidth / stringWidth;
                    var newFontSize = Math.floor(30 * widthRatio);
                    var elementHeight = (chart.innerRadius * 2);

                    // Pick a new font size so it will not be larger than the height of label.
                    var fontSizeToUse = Math.min(newFontSize, elementHeight);

                    //Set font settings to draw it correctly.
                    ctx.textAlign = 'center';
                    ctx.textBaseline = 'middle';
                    var centerX = ((chart.chartArea.left + chart.chartArea.right) / 2);
                    var centerY = ((chart.chartArea.top + chart.chartArea.bottom) / 2);
                    ctx.font = fontSizeToUse + "px " + fontStyle;
                    ctx.fillStyle = color;

                    //Draw text in center
                    ctx.fillText(txt, centerX, centerY);
                }
            }
        });/**/


        var config = {
            type: 'doughnut',
            data: {

                datasets: [{
                    data: [heart, temper],
                    backgroundColor: ["#36a2eb", "orange", "#ffffff",],
                    hoverBackgroundColor: ["#36a2eb", "orange", "#ffffff"], borderWidth: 0
                }
                ]
            },
            options: {
                cutoutPercentage: 90,
                elements: {
                    center: {
                        text: heart, color: '#FFf', // Default is #000000
                        fontStyle: 'Arial',
                        // Default is Arial
                        fontSize: 8,
                        sidePadding: 60 // Defualt is 20 (as a percentage)
                    }
                }
            }
        };


        var ctx = document.getElementById("myChart");
        this.myChart = new Chart(ctx, config);

        // chartname.data.datasets[0].data[2]=70;
        // chartname.update();
        // myChart.data.datasets[0].data[2] = 800;
        this.myChart.update();


        this.lineChart = new Chart(this.lineCanvas.nativeElement, {

            type: 'line',
            data: {
                labels: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
                datasets: [
                    {

                        fill: false,
                        lineTension: 0.1,
                        backgroundColor: "rgba(221,10,66,0.4)",
                        borderColor: "rgba(221,10,66,1)",
                        data: heartarray

                    }

                ]
            },
            options: {
                legend: {
                    display: false
                },
                tooltips: {
                    callbacks: {
                        label: function (tooltipItem) {
                            console.log(tooltipItem)
                            return tooltipItem.yLabel;
                        }
                    }
                },
                scales: {
                    xAxes: [{
                        display: true,
                        gridLines: {
                            drawOnChartArea: false
                        }
                    }],
                    yAxes: [{
                        display: true,
                        gridLines: {
                            drawOnChartArea: false
                        },
                        ticks: {
                            beginAtZero: true,
                            steps: 20,
                            stepValue: 5
                        }
                    }]
                }
            }
        }



        );
        this.myChart.update();

        this.lineChart = new Chart(this.bloodPressure.nativeElement, {

            type: 'line',
            data: {
                labels: [1,2,3,4,5,6,7,8,9,20],
                datasets: [
                    {

                        fill: false,
                        lineTension: 0.1,
                        backgroundColor: "rgba(221,10,66,0.4)",
                        borderColor: "rgba(221,10,66,1)",

                        data:[65, 59, 80, 81, 56, 55, 40],


                    }

                ]
            },
            options: {
                legend: {
                    display: false
                },
                tooltips: {
                    callbacks: {
                        label: function (tooltipItem) {
                            console.log(tooltipItem)
                            return tooltipItem.yLabel;
                        }
                    }
                },
                scales: {
                    xAxes: [{
                        display: true,
                        gridLines: {
                            drawOnChartArea: false
                        }
                    }],
                    yAxes: [{
                        display: true,
                        gridLines: {
                            drawOnChartArea: false
                        }
                    }]
                }
            }
        }



        );

        this.lineChart = new Chart(this.tempurature.nativeElement, {

            type: 'line',
            data: {
                labels: [1,2,3,4,5,6,7,8,9,10],
                datasets: [
                    {

                        fill: false,
                        lineTension: 0.1,
                        backgroundColor: "rgba(221,10,66,0.4)",
                        borderColor: "rgba(221,10,66,1)",

                        data:tempArray,


                    }

                ]
            },
            options: {
                legend: {
                    display: false
                },
                tooltips: {
                    callbacks: {
                        label: function (tooltipItem) {
                            console.log(tooltipItem)
                            return tooltipItem.yLabel;
                        }
                    }
                },
                scales: {
                    xAxes: [{
                        display: true,
                        gridLines: {
                            drawOnChartArea: false
                        }
                    }],
                    yAxes: [{
                        display: true,
                        gridLines: {
                            drawOnChartArea: false
                        },
                        ticks: {
                            beginAtZero: true,
                            steps: 10,
                            stepValue: 5
                        }
                    }]
                }
            }
        }



        );
    }





}
