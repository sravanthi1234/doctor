import { Component, NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { BluetoothSerial } from '@ionic-native/bluetooth-serial';
import { AlertController, LoadingController } from 'ionic-angular';
import { HttpClient, HttpHeaders } from '@angular/common/http';

/**
 * Generated class for the AdddevicePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-adddevice',
  templateUrl: 'adddevice.html',
})
export class AdddevicePage {
  output: any;
  message: String;
  responseTxt: any;
  unpairedDevices: any;
  pairedDevices: any;
  statusMessage: string;
  gettingDevices: Boolean;
  array: any;
  usertype: any;
  deviceaddress:any;
  constructor(public navCtrl: NavController, public navParams: NavParams,public http: HttpClient, public bluetoothSerial: BluetoothSerial, private alertCtrl: AlertController, private ngZone: NgZone) {
    this.usertype = this.navParams.get('usertype')
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AdddevicePage');
    this.bluetoothSerial.enable();
  }

  startScanning() {
    this.pairedDevices = null;
    this.unpairedDevices = null;
    this.gettingDevices = true;
    this.bluetoothSerial.discoverUnpaired().then((success) => {
      this.unpairedDevices = success;
      this.gettingDevices = false;
      success.forEach(element => {

      });
    },
      (err) => {
        console.log(err);
      })

    this.bluetoothSerial.list().then((success) => {
      this.pairedDevices = success;
    },
      (err) => {

      })
  }

  success = (data) => alert(data);
  fail = (error) => alert(error);


  selectDevice(address: any) {

    let alert = this.alertCtrl.create({
      title: 'Connect',
      message: 'Do you want to connect with?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Connect',
          handler: () => {
            this.deviceaddress = address;
            this.bluetoothSerial.connect(address).subscribe(this.success, this.fail);

          }
        }
      ]
    });
    alert.present(

    );

  }

  disconnect() {
    let alert = this.alertCtrl.create({
      title: 'Disconnect?',
      message: 'Do you want to Disconnect?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            alert.dismiss();
          }
        },
        {
          text: 'Disconnect',
          handler: () => {
            this.bluetoothSerial.disconnect();
            this.gettingDevices = null;
          }
        }
      ]
    });
    alert.present();
  }


  data() {
    setInterval(() => {
      this.read1();
    }, 3000);


  }


  read() {
    this.bluetoothSerial.read().then((data) => {
      // console.log(data);
      this.array = data.split(';');
      for (let i = 0; i < this.array.length; i++) {
            this.addData(this.array[i]);

      }

    })

  }

  read1() {
    this.ngZone.run(() => {
      this.read();
    })
  }

  addData(array){
    let senddata = array.split(',')
    console.log(this.deviceaddress);
    let body = {

        "heartbeat": senddata[0],
        "temperature":senddata[1],
        "humidity":senddata[2],
        "patientkey":"10"

    }

    let httpHeaders = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    let options = {
      headers: httpHeaders
    };

    this.http.post('http://ec2-54-84-142-57.compute-1.amazonaws.com:8080/devices/insertdevicedata',JSON.stringify(body),options).subscribe(data => {
      console.log(data);
    })
  }

}
