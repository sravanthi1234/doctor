import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AdddevicePage } from './adddevice';

@NgModule({
  declarations: [
    AdddevicePage,
  ],
  imports: [
    IonicPageModule.forChild(AdddevicePage),
  ],
})
export class AdddevicePageModule {}
