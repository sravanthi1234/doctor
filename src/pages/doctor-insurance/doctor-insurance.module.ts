import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DoctorInsurancePage } from './doctor-insurance';

@NgModule({
  declarations: [
    DoctorInsurancePage,
  ],
  imports: [
    IonicPageModule.forChild(DoctorInsurancePage),
  ],
})
export class DoctorInsurancePageModule {}
