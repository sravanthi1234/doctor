import { Chart } from 'chart.js';
import { ModalPage } from './../pages/modal/modal';

import { FormsModule } from '@angular/forms';

import { SESSION_STORAGE, WebStorageService } from 'angular-webstorage-service';
import { DoctorInsurancePage } from './../pages/doctor-insurance/doctor-insurance';
import { PatientInsurancePage } from './../pages/patient-insurance/patient-insurance';
import { DoctorsPage } from './../pages/doctors/doctors';
import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler,ModalController, NavParams } from 'ionic-angular';
import { MyApp } from './app.component';


import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import {LoginPage} from '../pages/login/login';

import { HttpClientModule } from '@angular/common/http';
import { PatientsPage } from '../pages/patients/patients';
import { AppointmentsPage } from '../pages/appointments/appointments';

import { InsurancePage } from "../pages/insurance/insurance";
import { StorageServiceModule} from 'angular-webstorage-service';
import { IonicStorageModule } from '@ionic/storage';
import { GlobalvarsProvider } from '../providers/globalvars/globalvars';
import { CalendarModule } from "ion2-calendar";
import {SignUpPage} from '../pages/sign-up/sign-up';
import { AddUserPage } from '../pages/add-user/add-user';
import { DemoProvider } from '../providers/demo/demo';
import { BluetoothSerial } from '@ionic-native/bluetooth-serial';
import { DataProvider } from '../providers/data/data';
// import * as CanvasJS from '../canvasjs-2.3.1/canvasjs.min.js';

@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    LoginPage,
    TabsPage,
    DoctorsPage,
    PatientsPage,
    AppointmentsPage,
    InsurancePage,
    PatientInsurancePage,
    DoctorInsurancePage,
    SignUpPage,
    ModalPage,
    AddUserPage
 ],
  imports: [
    FormsModule,

    BrowserModule,
    StorageServiceModule,
    HttpClientModule,
    CalendarModule,
    IonicStorageModule.forRoot(),
    IonicModule.forRoot(MyApp,{tabsPlacement: 'top'}),

  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    LoginPage,
    TabsPage,
    DoctorsPage,
    PatientsPage,
    AppointmentsPage,
    InsurancePage,
    PatientInsurancePage,
    DoctorInsurancePage,
    SignUpPage,
    ModalPage,
    AddUserPage

  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    GlobalvarsProvider,
    DemoProvider,
    BluetoothSerial,
    DataProvider
  ]
})
export class AppModule {

}
