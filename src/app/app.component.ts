import { GlobalvarsProvider } from './../providers/globalvars/globalvars';
import { Component, ViewChild } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { NavController, NavParams,AlertController,ModalController } from 'ionic-angular';
import { Nav } from 'ionic-angular';

import { TabsPage } from '../pages/tabs/tabs';
import {LoginPage} from '../pages/login/login';
import { InsurancePage } from '../pages/insurance/insurance';
import { Storage } from '@ionic/storage';
import { analyzeAndValidateNgModules } from '@angular/compiler';
import { MenuController } from 'ionic-angular'
import { ModalPage } from '../pages/modal/modal';
import { AddUserPage } from '../pages/add-user/add-user';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  userName:any;
  user:any;

  @ViewChild(Nav) nav: Nav;

  rootPage:any = LoginPage;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen,public alertCtrl: AlertController,private storage: Storage,public menuCtrl: MenuController,public modalCtrl: ModalController) {
   
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }
  openModal() {
    let myModal = this.modalCtrl.create(ModalPage);
    myModal.present();
  }
 
  
}
