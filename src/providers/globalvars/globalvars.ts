import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
/*
  Generated class for the GlobalvarsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class GlobalvarsProvider {
  user:any
  constructor(public http: HttpClient,private storage: Storage) {
    console.log('Hello GlobalvarsProvider Provider');
  }

  getUser(){
    this.storage.get('userName').then((val) => {
      this.user = val
    });
    return this.user;
  }

}
